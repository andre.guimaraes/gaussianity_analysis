import pandas as pd
import numpy as np
from os import listdir, system
from os.path import isfile, join
from ligo.lw import utils, ligolw
import argparse
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

def bankParser(bankFile):
    def myEval(val):
        if val != '':
            try:
                return eval(val)
            except:
                return val
        else:
            return np.nan

    class ContentHandler(ligolw.LIGOLWContentHandler):
        pass
    print("Loading Document")
    doc = utils.load_filename(bankFile, contenthandler=ContentHandler)
    print("Inferring SvdBin from Filename")
    svdBin = bankFile.split('/')[-1].split('-')[1].split('_')[0]
    print("Finding Banks in Files")
    bankIs = [i for i, row in enumerate(doc.childNodes[0].childNodes) if row.Name == 'sngl_inspiral:table']
    print("Reading Column Names")
    columns = []
    for row in doc.childNodes[0].childNodes[bankIs[0]].childNodes:
        try:
            columns += [row.Name]
        except:
            pass
    print("Parsing Data")
    pcdata = doc.childNodes[0].childNodes[bankIs[0]].childNodes[-1].pcdata
    pcdata = pcdata.replace("\t","").split("\n")
    pcdata = [data for data in pcdata if len(data) > 1]
    print("Initializing DataFrame as Dict")
    bank = {col:[] for col in columns}
    for i, col in enumerate(columns):
        bank[col] = [myEval(data.split(",")[i]) for data in pcdata[:-1] if len(data) > 1]
    print("Turning to DataFrame")
    bank = pd.DataFrame(bank)
    bank['SVD_Bin'] = [svdBin]*len(bank)
    
    return bank

parser = argparse.ArgumentParser()

parser.add_argument("--inDir", help="Path to directory where all banks svd xml's are stored")
parser.add_argument("--outFile", help="Path to directory for all output csv files")
parser.add_argument("--tempDir", help="Temporary Directory for intermediary files")
parser.add_argument("--tag", help="Tag to identify temporary files")


args = parser.parse_args()
print("Reading Command Line Arguments")
inDir = args.inDir
outFile = args.outFile
tempDir = args.tempDir
tag = args.tag
print("Initializing")
if not (inDir.endswith("/")):
    inDir = inDir + "/"
if not (tempDir.endswith("/")):
    tempDir = tempDir + "/"
if tag == None:  
    tag = "Split_Bank_Map_" + str(np.random.randint(1000000000))
    
bankFiles = sorted([inDir + f for f in listdir(inDir) if f.endswith('.xml.gz')])

saveN = 0

for bankFile in bankFiles:
    print("Interpreting " + bankFile)
    bankParser(bankFile).to_csv(tempDir + tag + "_" + str(saveN) + ".csv")
    saveN += 1

banks = pd.read_csv(tempDir + tag + "_" + str(0) + ".csv", low_memory=False)
for i in range(1, saveN):
    banks = banks.append(pd.read_csv(tempDir + tag + "_" + str(i) + ".csv", low_memory=False))
banks = banks.drop_duplicates(subset = 'Gamma0')
banks.to_csv(outFile)
for i in range(saveN):
    system("rm " + tempDir + tag + "_" + str(i) + ".csv")

