import pandas as pd
import numpy as np
from os import listdir, system
from os.path import isfile, join
from ligo.lw import utils
from ligo.lw import ligolw
import argparse

def bankParser(bankFile):
    def myEval(val):
        if val != '':
            return eval(val)
        else:
            return np.nan

    class ContentHandler(ligolw.LIGOLWContentHandler):
        pass
    print("Loading Document")
    doc = utils.load_filename(bankFile, contenthandler=ContentHandler)
    print("Separating Table")
    ligolwTable = doc.childNodes[0].childNodes[0]
    print("Separating Columns")
    columns = ligolwTable.childNodes[:-1]
    print("Separating Data")
    data = ligolwTable.childNodes[-1]
    print("Gathering Column Names")
    columnNames = [column.Name for column in columns]
    print("Loading Raw Data")
    dataRaw = data.pcdata
    print("Editing Data into array")
    dataRaw = dataRaw.replace('\t','').split('\n')[1:-1]
    print("Splitting Data lines")
    dataRaw = [line.split(',') for line in dataRaw]
    print("Evaluating Data Entries")
    dataRaw = [[myEval(value) for value in line] for line in dataRaw]
    print("Transposing Data Matrix")
    dataRawT = list(zip(*dataRaw))
    print("Creating Bank Dict")
    bank = {columnNames[i]:dataRawT[i] for i in range(len(columnNames))}
    print("Creating Bank DataFrame")
    bankDF = pd.DataFrame(bank)
    return bankDF

parser = argparse.ArgumentParser()

parser.add_argument("--inDir", help="Path to directory where all banks xml's are stored")
parser.add_argument("--outDir", help="Path to directory for all output csv files")

args = parser.parse_args()
print("Reading Command Line Arguments")
inDir = args.inDir
outDir = args.outDir

print("Initializing")
if not (inDir.endswith("/")):
    inDir = inDir + "/"
if not (outDir.endswith("/")):
    outDir = outDir + "/"

    
columnsOfInterest = ['Gamma0', 'eta', 'event_id', 'f_final', 'mass1', 'mass2', 'mchirp', 'mtotal', 'sigmasq', 'snr', 'spin1x', 'spin1y', 'spin1z', 'spin2x', 'spin2y', 'spin2z', 'tau0', 'tau3', 'template_duration']

bankFiles = [inDir + f for f in listdir(inDir) if f.endswith('.xml.gz')]

for bankFile in bankFiles:
    print(bankFile)
    bankParameters = {col:[] for col in columnsOfInterest}
    bankParameters['apx'] = []
    banks = bankParser(bankFile)
    if 'bbh_low_q' in bankFile:
        banks['apx'] = ['bbh_low_q']*len(banks)
    elif 'bns' in bankFile:
        banks['apx'] = ['bns']*len(banks)
    elif 'imbh_low_q' in bankFile:
        banks['apx'] = ['imbh_low_q']*len(banks)
    elif 'nsbh' in bankFile:
        banks['apx'] = ['nsbh']*len(banks)
    elif 'other_bbh' in bankFile:
        banks['apx'] = ['other_bbh']*len(banks)
    else:
        banks['apx'] = ['None']*len(banks)
    
    banks.drop_duplicates().to_csv(outDir + bankFile.split("/")[-1].split(".xml.gz")[0] + ".csv")


