#!/usr/bin/env python3
import pandas as pd
import numpy as np
from ligo.lw import utils
from ligo.lw import ligolw
import argparse

def parsePCDAta(pcdata):
    data = pcdata.replace("\t","").split("\n")[1:-1]
    data = [line.split(',') for line in data]
    data = [[eval(d) for d in line if d != ""] for line in data]
    return data

ifoOrder = ['H1','L1','V1']

parser = argparse.ArgumentParser()

parser.add_argument("--inFile", help="Path input xml file")
parser.add_argument("--outFile", help="Path output csv file")

args = parser.parse_args()
print("Reading Command Line Arguments")
inFile = args.inFile
outFile = args.outFile

class ContentHandler(ligolw.LIGOLWContentHandler):
    pass

doc = utils.load_filename(inFile, contenthandler=ContentHandler, verbose = True)

table_data = doc.childNodes[0].childNodes[4]
columnNames = [col.Name for col in table_data.childNodes[:-1]]
columnNames[-1] = 'ifo'
values = parsePCDAta(table_data.childNodes[-1].pcdata)
for i in range(len(values)):
    values[i][-1] = ifoOrder[values[i][-1]]
values = np.array(values).T
df = pd.DataFrame({columnNames[i]:values[i] for i in range(len(columnNames))}).drop(["process:process_id","segment_id","start_time_ns","end_time_ns"], axis = 'columns')
df.to_csv(outFile)

