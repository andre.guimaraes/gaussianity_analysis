import pandas as pd
import numpy as np
from os import listdir, system, environ
from os.path import isfile, join
from ligo.lw import utils
from ligo.lw import ligolw
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
environ["GSTLAL_FIR_WHITEN"] = '0'
import argparse

def myEval(val):
    if val != '' and val != 'nan':
        return eval(val)
    else:
        return np.nan
    
def parseTriggerFiles(fileName):
    class ContentHandler(ligolw.LIGOLWContentHandler):
        pass
    print("Loading Document")
    doc = utils.load_filename(file, contenthandler=ContentHandler, verbose = True)
    tables = doc.childNodes[0].childNodes
    triggerPDs = []
    for table in tables:
        columns = table.childNodes[:-1]
        columnNames = [column.Name for column in columns]
        if "mass1" in columnNames:
            data = table.childNodes[-1]
            dataRaw = data.pcdata
            dataRaw = dataRaw.replace('\t','').replace("H1,","H1:").replace("L1,","L1:").replace("V1,","V1:").split('\n')[1:-1]
            dataRaw = [line.split(',') for line in dataRaw]
            dataRaw = [[myEval(value) for value in line] for line in dataRaw]
            if len(dataRaw) == 0 or len(dataRaw[0]) == 0:
                triggerFile = {columnNames[i]:[] for i in range(len(columnNames))}
            else:
                dataRawT = list(zip(*dataRaw))
                triggerFile = {columnNames[i]:dataRawT[i] for i in range(len(columnNames))}
            triggerPDs += [pd.DataFrame(triggerFile)]
    finalPD = triggerPDs[0]
    for i in range(1,len(triggerPDs)):
        finalPD = finalPD.append(triggerPDs[i])
    print(str(len(finalPD)) + " Triggers Recovered") 
    return finalPD

parser = argparse.ArgumentParser()

parser.add_argument("--inDir", help="Path to directory where all trigger xml's are stored")
parser.add_argument("--outFile", help="Path output csv file")
parser.add_argument("--tempDir", help="Path to temporary file Directory")

args = parser.parse_args()
print("Reading Command Line Arguments")
inDir = args.inDir
outFile = args.outFile
tempDir = args.tempDir

print("Initializing")
if not (inDir.endswith("/")):
    inDir = inDir + "/"
if not (tempDir.endswith("/")):
    tempDir = tempDir + "/"

subfolders = [inDir + f for f in listdir(inDir) if len(f) == 5 and not(f.startswith('.'))]
allfiles = sorted([subfolder + "/" + f for subfolder in subfolders for f in listdir(subfolder) if f.endswith('.xml.gz')])

tag = str(np.random.randint(0, high = 10000000))

print(str(len(allfiles)) + " Total files")
triggers = pd.DataFrame({})
i = 0
saveN = 0
Ntotal = 0
print("Looping through all files to read triggers")
for file in allfiles:
    svd_bin = file.split('/')[-1].split('-')[1].split('_')[0]
    system('clear')
    print(str(i) + " / " + str(len(allfiles)))
    dfTemp = parseTriggerFiles(file)
    dfTemp['SVD Bin'] = [svd_bin]*len(dfTemp)
    if len(triggers) == 0:
        triggers = dfTemp.copy()
    else:
        triggers = triggers.append(dfTemp.copy())
    Ntotal += len(dfTemp)
    print("Number of triggers found: " + str(Ntotal))
    print("Number of triggers currently: " + str(len(triggers)))
    i += 1
    if len(triggers) >= 1000000:
        print("Saving Checkpoint")
        triggers.to_csv(tempDir + tag + "_" + str(saveN) + ".csv")
        saveN += 1
        triggers = pd.DataFrame({})

triggers.to_csv(tempDir + tag + "_" + str(saveN) + ".csv")
saveN += 1
triggers = pd.DataFrame({})
print("Joining all csvs into one")
for i in range(saveN):
    if len(triggers) == 0:
        triggers = pd.read_csv(tempDir + tag + "_" + str(i) + ".csv", low_memory=False)
    else:
        triggers = triggers.append(pd.read_csv(tempDir + tag + "_" + str(i) + ".csv", low_memory=False))

        
#triggers.to_csv(outFile)

