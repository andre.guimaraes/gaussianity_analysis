import sqlite3
import pandas as pd
import numpy as np
import warnings
import argparse
from os import listdir, system, environ

warnings.simplefilter(action='ignore', category=FutureWarning)
environ["GSTLAL_FIR_WHITEN"] = '0'

parser = argparse.ArgumentParser()

parser.add_argument("--inFile", help="Path to directory where all trigger sqlite's are stored")
parser.add_argument("--outFile", help="Path output csv file")
parser.add_argument("--bankMapFile", help = "Path to SVD Bank Map csv")

args = parser.parse_args()
print("Reading Command Line Arguments")
inFile = args.inFile
outFile = args.outFile
bankMapFile = args.bankMapFile

print("Connecting to sqlite database")
con = sqlite3.connect(inFile)

cur = con.cursor()

print("Reading Coinc Event Table")
table = 'coinc_event'
cur = con.execute('select * from ' + table)
names = list(map(lambda x: x[0], cur.description))
df_coinc = {name:[] for name in names}

for row in cur.execute('SELECT * FROM '  + table + ";"):
    for i in range(len(names)):
        df_coinc[names[i]] += [row[i]]
df_coinc = pd.DataFrame(df_coinc)


print("Reading Coinc Event Map Table")
table = 'coinc_event_map'
cur = con.execute('select * from ' + table)
names = list(map(lambda x: x[0], cur.description))
df_coinc_map = {name:[] for name in names}

for row in cur.execute('SELECT * FROM '  + table + ";"):
    for i in range(len(names)):
        df_coinc_map[names[i]] += [row[i]]
df_coinc_map = pd.DataFrame(df_coinc_map)

print("Reading Single Inspiral Table")
table = 'sngl_inspiral'
cur = con.execute('select * from ' + table)
names = list(map(lambda x: x[0], cur.description))
df_sngl_inspiral = {name:[] for name in names}

for row in cur.execute('SELECT * FROM '  + table + ";"):
    for i in range(len(names)):
        df_sngl_inspiral[names[i]] += [row[i]]
df_sngl_inspiral = pd.DataFrame(df_sngl_inspiral)

print("Assigning Likelihood to Single Inspiral Events")
coinc_ids = df_coinc_map['coinc_event_id'].values
event_ids = df_coinc_map['event_id'].values
likelihoods = []
for i, row in df_sngl_inspiral.iterrows():
    if i % 1000 == 0:
        print("Assigning Likelihoods: Single Inspiral Row: " + str(i) + "/" + str(len(df_sngl_inspiral)))
    event_id = row['event_id']
    coinc_id = df_coinc_map.set_index('event_id').loc[event_id]['coinc_event_id']
    if not isinstance(coinc_id, np.int64):
        coinc_id = coinc_id.iloc[0]
    likelihoods += [df_coinc.set_index('coinc_event_id').loc[coinc_id]['likelihood']]
df_sngl_inspiral['likelihood'] = likelihoods

if bankMapFile != None:
    print("Reading SVD Bin Mapping and assigning bank parameters to Single Inspital Events")
    df_map_Bank = pd.read_csv(bankMapFile, low_memory = False)
    gamma0s = df_map_Bank['Gamma0'].values.tolist()
    df_map_Bank = df_map_Bank.set_index('Gamma0')

    svdBins = []
    tau0s = []
    tau3s = []
    for i, row in df_sngl_inspiral.iterrows():
        if row['Gamma0'] in gamma0s:
            svdBins += [df_map_Bank.loc[row['Gamma0']]['SVD_Bin']]
            tau0s += [df_map_Bank.loc[row['Gamma0']]['tau0']]
            tau3s += [df_map_Bank.loc[row['Gamma0']]['tau3']]
        else:
            svdBins += ['N/A']
            tau0s += ['N/A']
            tau3s += ['N/A']
    df_sngl_inspiral['SVD_Bin'] = svdBins
    df_sngl_inspiral['tau0'] = tau0s
    df_sngl_inspiral['tau3'] = tau3s
print("Single Inspital Events")
df_sngl_inspiral.to_csv(outFile)


